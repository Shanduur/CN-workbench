// based on https://www.codeproject.com/Tips/1040097/Create-simple-http-server-in-Java
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.IOException;
import java.io.OutputStream;
import com.sun.net.httpserver.Headers;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class HomeHandler implements HttpHandler {

    @Override

    public void handle(HttpExchange he) throws IOException {
        Headers headers = he.getRequestHeaders();
        Set<Map.Entry<String, List<String>>> entries = headers.entrySet();
        String response = "Server started successfully. Port number is: " + LabServer.port + ".\n\n";
        for (Map.Entry<String, List<String>> entry : entries)
            response += entry.toString() + "\n";
        he.sendResponseHeaders(200, response.length());
        OutputStream os = he.getResponseBody();
        os.write(response.toString().getBytes());
        os.close();
    }
}