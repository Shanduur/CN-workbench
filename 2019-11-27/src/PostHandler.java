// based on https://www.codeproject.com/Tips/1040097/Create-simple-http-server-in-Java
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class PostHandler implements HttpHandler {
	@SuppressWarnings("unchecked")
    public static void parseQuery(String query, Map<String, Object> parameters) throws UnsupportedEncodingException {
        if (query != null) {
            String pairs[] = query.split("[&]");
            for (String pair : pairs) {
                String param[] = pair.split("[=]");
                String key = null;
                String value = null;
                if (param.length > 0) {
                    key = URLDecoder.decode(param[0],
                            System.getProperty("file.encoding"));
                }
                if (param.length > 1) {
                    value = URLDecoder.decode(param[1],
                            System.getProperty("file.encoding"));
                }
                if (parameters.containsKey(key)) {
                    Object obj = parameters.get(key);
                    if (obj instanceof List<?>) {
                        List<String> values = (List<String>) obj;
                        values.add(value);
                    } else if (obj instanceof String) {
                        List<String> values = new ArrayList<String>();
                        values.add((String) obj);
                        values.add(value);
                        parameters.put(key, values);
                    }
                } else {
                    parameters.put(key, value);
                }
            }
        }
    }
    @Override
	@SuppressWarnings("unchecked")
    public void handle(HttpExchange he) throws IOException {
        // parse request
        Map<String, Object> parameters = new HashMap<String, Object>();
        InputStreamReader isr = new InputStreamReader(he.getRequestBody(), "utf-8");
        BufferedReader br = new BufferedReader(isr);
        String query = br.readLine();
        parseQuery(query, parameters);

        // send response
        String response = "<html>\n" +
                "   <body>\n" +
                "      <form action = \"/post\" method = \"POST\">\n" +
                "         Name: <input type = \"text\" name = \"Name\" /><br/>" +
                "         Age:  <input type = \"text\" name = \"Age\" /><br/>" +
                "         City: <input type = \"text\" name = \"City\" /><br/>" +
                "         Major: <input type = \"text\" name = \"Major\" /><br/>" +
                "         <input type = \"submit\" value=\"Submit\"/><br/>" +
                "      </form>\n" +
                "   </body>\n" +
                "</html>";
        response += "POST:\n";
        for (String key : parameters.keySet())
            response += key + " = " + parameters.get(key) + "<br/>";
        he.sendResponseHeaders(200, response.length());
        OutputStream os = he.getResponseBody();
        os.write(response.toString().getBytes());
        os.close();
    }
}