// based on https://www.codeproject.com/Tips/1040097/Create-simple-http-server-in-Java
import com.sun.net.httpserver.HttpServer;
import java.net.InetSocketAddress;
import java.io.IOException;
public class LabServer implements PortNumber{
    public int PortNo()
    {
        return port;
    }
    public static void main (String[] args) {
        System.out.println("*** Simple web server ***");
        LabServer ls = new LabServer();
        try {
            HttpServer server = HttpServer.create(new InetSocketAddress(ls.PortNo()), 0);
            System.out.println("Server started at port number " + ls.PortNo() + ".");
            server.createContext("/home", new HomeHandler());
            server.createContext("/get", new GetHandler());
            server.createContext("/post", new PostHandler());
            server.setExecutor(null);
            server.start();
        }
        catch (IOException ioe) {
            System.out.println("Error");
        }
    }
}
