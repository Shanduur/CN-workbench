#!/bin/bash

# String operations
str1='Dama Tu Jest'

echo $str1

# Operations on strings acting as integer
num1=5

let 'num1-=7'

echo $num1

# Hexadecimal
let 'hex=0x0A'

echo $hex

# Binary
let 'bin=2#101'

echo $bin

# Output to string
files=`ls -al`

echo '$files'
echo "$files"

# Path
echo $0 

# Show args 1, 2 and 3
echo $1 $2 $3

# How many args
echo $#

# all args as non-separate str
echo $*

#all args as separate str
echo $@

# Previous command (0 if no err)
echo $?

ret=$?

# LOOPS, I NEED LOOPS HOOMAN
if (ret=='0') then
    echo 'great'
elif (ret=='1') then
    echo 'not so great'
else
    echo 'i dont hnow what you want me to do'
fi

# Loop if using test command
if test $num1 -eq 12
then 
    echo true
else
    echo false
fi

# using build in Bash function
if [ $num1 -eq 12 ]
    then 
        echo true
elif [ $num1 -eq -2 ]
    then
        echo true
else
    echo false
fi

# for
days="Mon Tue We"
for i in $days
do 
    echo $i
done

for i in ~/*
do
    echo $i
done